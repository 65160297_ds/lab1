/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1;

/**
 *
 * @author Name
 */
import java.util.Arrays;
import java.util.Scanner;

public class ArrayManipulation{
    public static void main(String[] args) {
        int sum = 0;
        double maxvalues = 0;
        Scanner kb = new Scanner(System.in);
        int[] numbers = {5, 8, 3, 2, 7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[4];
        System.out.println("Element of the numbers array:");
        for(int i = 0; i < numbers.length;i++){
            System.out.println(numbers[i]);
            sum += numbers[i];
        }
        System.out.println("Element of the names array:");
        for(int i = 0; i < names.length;i++){
            System.out.println(names[i]);
        }
        System.out.println("Input the values in array:");
        for(int i = 0; i < values.length;i++){
            double val = kb.nextDouble();
            values[i] = val;
        }
        for(int i = 0;i < values.length;i++){
            if(values[i]>maxvalues){
                maxvalues = values[i];
            }
        }
        System.out.println("Sum of numbers : "+sum);
        System.out.print("maximum Element of the values array: ");
        System.out.println(maxvalues);
        String[] reversedNames = new String[names.length];
        for(int i = 0; i < names.length;i++){
            reversedNames[i] = names[names.length - 1 - i];
        }
        System.out.println("Print the elements of the reversedNames array :");
        for(int i = 0; i < names.length;i++){
            System.out.println(reversedNames[i]);
        }
        Arrays.sort(numbers);
        System.out.println("Print the sorted numbers array :");
        for(int i = 0;i < numbers.length;i++){
            System.out.println(numbers[i]);
        }
    }
}
