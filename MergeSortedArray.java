/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab1;

/**
 *
 * @author Name
 */
import java.util.Arrays;

public class MergeSortedArray {
    public static void main(String[] args) {
        int[] nums1 = {1,2,3,0,0,0};
        int m = 3;
        int[] nums2 = {2,5,6};
        int n = 3;
        int[] arr = new int[m+n];
        MSA(nums1, nums1, nums2, m, n);
        System.out.println(Arrays.toString(arr));
    }
    public static void MSA(int[] arr,int[] nums1,int[] nums2,int m,int n){
        int k = 0;
        for(int i = 0;i < m;i++){
            arr[k] = nums1[i];
            k++;
        }
        for(int j = 0;j < n;j++){
            arr[k] = nums2[j];
            k++;
        }
        int b = arr.length;
        for (int i = 0; i < arr.length-1; i++) {
            int temp = arr[i];
            arr[i] = arr[i + 1];
            arr[i+ 1] = temp;    
        }
    }
}
